var ready;

<<<<<<< HEAD
ready = function(){
    console.log("test.js loading");

/*
Improvements to be made
Gérer mistypes
Gérer non existences
Gérer erreurs
*/

$(function() {
    function display(type, msg) {
        console.log(type.toString().toUpperCase() + " : " + msg.toString())
    }

    var DecisionTree = {
        initial: "",
        choimageices: {},
        status: "",
        result: {},

        reset: function() {
            this.initial = "",
                this.choices = {},
                this.status = "",
                this.result = {}
        },

        init: function(initial, choices, status, result) {
            this.initial = initial.toString();
            this.choices = choices;
            this.status = status.toString();
            this.result = {};
            $(".choice-answer").click($.proxy(function(e) {
                e.preventDefault();
                this.progress($(e.currentTarget).data("answer").toString());
                this.display_status();
            }, this));
        },

        push_choice: function(id, text_choice, img_choice, yes_id, no_id) {
            this.choices[id] = {
                text: text_choice,
                img: img_choice,
                yes: yes_id,
                no: no_id
            };
        },

        progress: function(answer) {
            display("info", "Answer = " + answer.toString() + " | Status = " + this.status)
            if (answer == "yes") {
                this.result[this.status] = answer;
                this.status = this.choices[this.status].yes_id;
                this.display_html();
                display("info", "New status = " + this.status);
                this.is_end();
            } else if (answer == "no") {
                this.result[this.status] = answer;
                this.status = this.choices[this.status].no_id;
                this.display_html();
                display("info", "New status = " + this.status);
                this.is_end();
            } else {
                display("error", "Answer not recognized");
            };
            display("info", "status = " + this.status);
        },

        is_end: function() {
            if (this.status == "end") {
                $(".choice-answer").unbind("click");
                $(".choice-answer").attr("href", "/users/sign_up");
                this.write_cookie();
                return true;
            } else {
                return false;
            }
        },

        write_cookie: function() {
            console.log(this.result);
            Cookies.set("quiz-result", JSON.stringify(this.result));
            console.log(Cookies.get("quiz-result"));
        },

        display_status: function() {
            display("Status", "Status : " + this.status + " If Yes : " + this.choices[this.status].yes + " If No : " + this.choices[this.status].no);
            display("Status", "text : " + this.choices[this.status].text + " img : " + this.choices[this.status].img);
            var result_string = "";
            for (var i in this.result) {
                result_string += i + " : " + this.result[i].toString() + " | "
            }
            display("Status", "Result : " + result_string)
        },

        display_html: function() {
            $("#choice-detail").text(this.choices[this.status].text.toString());
            var newSrc = "/assets/quiz/" + this.choices[this.status].img.toString();
            $.ajax({
                url: newSrc,
                type: "GET",
                headers: {
                    "X-TOKEN": 'xxxxx'
                }
            }).done(function() {
                $("#choice-img").attr("src", newSrc);
            }).fail(function() {
                console.log("Ajax failed");
            });

        },

        get_result: function() {
            if (this.is_end()) {
                return this.result;
            } else {
                display("error", "Decision Tree not complete");
            }
        }
    };

    var Timer = {
        time: 60,
        status: 0,
        interval: 1,
        color_start: 105,
        color_end: 15,
        progress: function() {
            if (this.status < this.time && this.status >= 0) {
                this.status += this.interval;
            } else {
                display("info", "timer end reached");
                clearInterval(this.loop);
            }
        },
        display_html: function() {
            var w = 100 - (this.status / this.time) * 100;
            var color = this.color_start - (this.color_start - this.color_end) * (this.status / this.time);
            //console.log(color);
            $("#timer-progress").width(w.toString() + "%");
            $("#timer-txt").text(Math.round(this.time - this.status).toString());
            $("#timer-txt").css({ left: 100 - w.toString() + "%" })
            $("#timer-progress").css("background-color", "hsl(" + Math.round(color) + ", 100%, 50%)")
        },
        info: function() {
            display("info", "Status: " + this.status.toString() + " | Time: " + this.time.toString() + " | Interval: " + this.interval.toString() + " | Ratio: " + ((this.status / this.time) * 100).toString() + "%");
        },
        play: function() {
            var timer_used = this;
            var loop = setInterval(function() {
                timer_used.progress();
                timer_used.display_html();
                //timer_used.info();
                if (timer_used.status >= timer_used.time) {
                    clearInterval(loop);
                    timer_used.info();
                }
            }, this.interval * 1000);
        }
    };


""
    var sport_tree = Object.create(DecisionTree);
    var sport_choices = { "paris": { text: "Habitez-vous Paris ?", img: "paris", yes_id: "woman", no_id: "woman" }, "woman": { text: "Etes-vous......une femme ?", img: "woman", yes_id: "student", no_id: "student" }, "student": { text: "...Etudiant ?", img: "student", yes_id: "agenda", no_id: "freelance" }, "freelance": { text: "...Freelance ?", img: "freelance", yes_id: "agenda", no_id: "startupper" }, "startupper": { text: "...Startupper ?", img: "startupper", yes_id: "agenda", no_id: "salaried" }, "salaried": { text: "...Salarié ?", img: "salaried", yes_id: "agenda", no_id: "agenda" }, "agenda": { text: "Avez-vous planifié certaines des Activités de la semaine qui vient sur une application mobile d'agenda ? (Google Agenda...)", img: "agenda", yes_id: "professional activities", no_id: "3hours" }, "professional activities": { text: "....des activités professionnelles ?", img: "professional activities", yes_id: "personal", no_id: "3hours" }, "personal": { text: "...Des activités personnelles? (ie Médecins, mariages...)", img: "personal_activities", yes_id: "friendly_activities", no_id: "3hours" }, "friendly_activities": { text: "...Activités amicales ou familiales ? (Sport, Cinéma, ...)", img: "friendly_activities", yes_id: "3hours", no_id: "3hours" }, "3hours": { text: "Avez-vous en moyenne des plages horaires non planifiées dans votre calendrier: + de 3h/hour ?", img: "3hours", yes_id: "6hours", no_id: "6hours" }, "6hours": { text: "... : =+ de 6h/hour ?", img: "6hours", yes_id: "google_calendar", no_id: "google_calendar" }, "google_calendar": { text: "L'application que vous utilisez le + est : Google Calendar ?", img: "google_calendar", yes_id: "metro", no_id: "apple_calendar" }, "apple_calendar": { text: "...Le Calendrier Apple ?", img: "apple_calendar", yes_id: "metro", no_id: "metro" }, "metro": { text: "Votre moyen de locomotion le + fréquent est: Le Métro ?", img: "metro", yes_id: "LittleGauge", no_id: "bike" }, "bike": { text: "Moyen de locomotion...le + fréquent : Le vélo ?", img: "bike", yes_id: "LittleGauge", no_id: "car" }, "car": { text: "Moyen de locomotion le + fréquent : La voiture ?", img: "car", yes_id: "LittleGauge", no_id: "LittleGauge" }, "LittleGauge": { text: "Comment évaluez vous votre pratique du sport: plutôt...Faible (à venir medium & high)?", img: "LittleGauge", yes_id: "tennis", no_id: "MediumGauge" }, "MediumGauge": { text: "...Pratique sport : fréquente ?", img: "MediumGauge", yes_id: "tennis", no_id: "HighGauge" }, "HighGauge": { text: "...Pratique sport : haut-niveau ?", img: "HighGauge", yes_id: "tennis", no_id: "tennis" }, "tennis": { text: "Avez-vous pratiqué au cours des 3 derniers mois du : Tennis ?", img: "tennis", yes_id: "squash", no_id: "squash" }, "squash": { text: "Avez-vous pratiqué au cours des 3 derniers mois du : squash ?", img: "squash", yes_id: "musculation", no_id: "musculation" }, "musculation": { text: "Pratiqué...3 derniers mois... Musculation ?", img: "musculation", yes_id: "Travel", no_id: "Travel" }, "Travel": { text: "Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…: par mois ?", img: "travel", yes_id: "travel2", no_id: "travel2" }, "travel2": { text: "Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…: par semaine ?", img: "travel", yes_id: "macOs", no_id: "macOs" }, "macOs": { text: "Système d’exploitation que vous utilisez le + : macOS ?", img: "macOs", yes_id: "iOs", no_id: "windows" }, "windows": { text: "Système d’exploitation que vous utilisez le + : Windows ?", img: "windows", yes_id: "iOs", no_id: "iOs" }, "iOs": { text: "OS Mobile que vous utilisez le + : iOS?", img: "ios", yes_id: "Free", no_id: "Free" }, "Free": { text: "Opérateur mobile : Free ?", img: "free", yes_id: "Coding", no_id: "Coding" }, "Coding": { text: "Avez-vous déjà... codé ?", img: "Coding", yes_id: "Back-end", no_id: "improvisation" }, "Front-end": { text: "plutôt front-end... ?", img: "Back-end", yes_id: "Back-end", no_id: "RubyOnRails" }, "Back-end": { text: "Full-Stack ?", img: "Front-end", yes_id: "RubyOnRails", no_id: "improvisation" }, "RubyOnRails": { text: "RubyOnRails ?", img: "RubyOnRails", yes_id: "sql", no_id: "sql" }, "sql": { text: "SQL ?", img: "sql", yes_id: "MachineLearning", no_id: "MachineLearning" }, "MachineLearning": { text: "MachineLearning ?", img: "MachineLearning", yes_id: "improvisation", no_id: "improvisation" }, "improvisation": { text: "Avez-vous déjà fait...: du théâtre d'improvisation ?", img: "improvisation", yes_id: "crowdfunding", no_id: "crowdfunding" }, "crowdfunding": { text: "Avez-vous déjà fait...: une campagne de Crowdfunding?", img: "crowdfunding", yes_id: "Mobile App Development", no_id: "Mobile App Development" }, "Mobile App Development": { text: "Avez-vous déjà fait...: une Application mobile ?", img: "Mobile_App_Development", yes_id: "cinema", no_id: "cinema" }, "cinema": { text: "Avez-vous déjà fait... au cours des 6 derniers mois...une sortie cinéma ?", img: "cinema", yes_id: "Trust2", no_id: "Trust2" }, "Trust2": { text: "Etes-vous d'accord pour partager informations précédentes avec la communauté NowFellows et être éventuellement mis en relation avec des personnes qui vous correspondent ?", img: "Trust2", yes_id: "Trust", no_id: "Trust" }, "Trust": { text: "Etes-vous intéressé pour partager/enseigner une compétence avec d'autres early-adopters de NowFellows lors d'un évènement à Paris Bastille ⛵ ?", img: "Trust", yes_id: "couple", no_id: "couple" }, "couple": { text: "Etes-vous en couple ? Pour vous partager des idées de sorties, activités, cadeaux...ou des propositions d'éventuelles opportunités de rencontre si ce n'est pas le cas. Seul le coach de NowFellows a accès à cette information", img: "couple", yes_id: "LearnToLearn", no_id: "LearnToLearn" }, "LearnToLearn": { text: "Intéressé par apprendre de nouvelles compétences ?", img: "LearnToLearn", yes_id: "end", no_id: "end" }, "end": { text: "Merci d'avoir répondu à ce questionnaire, cliquez sur une des 2 flèches pour découvrir la suite ;)", img: "end", yes_id: "end", no_id: "end" } };

    /*{
        "sport": {
            text: "Faites-vous régulièrement du sport?",
            img: "sport.png",
            yes: "sport-co",
            no: "sport-proposition"
        },
        "sport-co": {
            text: "Faites-vous plutôt du sport en équipe?",
            img: "sport-co.png",
            yes: "football",
            no: "squash"
        },
        "sport-proposition": {
            text: "Souhaitez-vous trouver qqun pour vous motiver à faire du sport ?",
            img: "sport-ami.png",
            yes: "sport-co",
            no: "end"
        },
        "football": {
            text: "Pratiquez-vous le Football ?",
            img: "foot.png",
            yes: "end",
            no: "end"
        },
        "squash": {
            text: "Pratiquez-vous le Squash ?",
            img: "squash.png",
            yes: "end",
            no: "end"
        },
        "end": {
            text: "Merci d'avoir participé au quiz, cliquez sur l'une des flèches pour vous inscrire et découvrir ce que nous vous réservons",
            img: "end.png",
            yes: "end",
            no: "end"
        }
    };*/
    var sport_initial = "paris";
    var sport_result = {};
    var sport_status = "paris";

    var timer_test = Object.create(Timer);
    timer_test.time = 180;
    timer_test.interval = 0.01;
    timer_test.status = 0;

    $('.choice-answer').click(function(e) {
        e.preventDefault();
        $('.choice-answer').unbind("click");
        sport_tree.init(sport_initial, sport_choices, sport_status, sport_result);
        sport_tree.display_html();
        timer_test.play();
    });
    console.log("test.js loaded");
});  //your js scripts go here
=======
ready = function() {
    console.log("test.js loading");

    /*
    Improvements to be made
    Gérer mistypes
    Gérer non existences
    Gérer erreurs
    */

    $(function() {
        function display(type, msg) {
            console.log(type.toString().toUpperCase() + " : " + msg.toString())
        }

        var DecisionTree = {
            initial: "",
            choimageices: {},
            status: "",
            result: {},

            reset: function() {
                this.initial = "",
                    this.choices = {},
                    this.status = "",
                    this.result = {}
            },

            init: function(initial, choices, status, result) {
                this.initial = initial.toString();
                this.choices = choices;
                this.status = status.toString();
                this.result = {};
                $(".choice-answer").click($.proxy(function(e) {
                    e.preventDefault();
                    this.progress($(e.currentTarget).data("answer").toString());
                    this.display_status();
                }, this));
            },

            push_choice: function(id, text_choice, img_choice, yes_id, no_id) {
                this.choices[id] = {
                    text: text_choice,
                    img: img_choice,
                    yes: yes_id,
                    no: no_id
                };
            },

            progress: function(answer) {
                display("info", "Answer = " + answer.toString() + " | Status = " + this.status)
                if (answer == "yes") {
                    this.result[this.status] = answer;
                    this.status = this.choices[this.status].yes_id;
                    this.display_html();
                    display("info", "New status = " + this.status);
                    this.is_end();
                } else if (answer == "no") {
                    this.result[this.status] = answer;
                    this.status = this.choices[this.status].no_id;
                    this.display_html();
                    display("info", "New status = " + this.status);
                    this.is_end();
                } else {
                    display("error", "Answer not recognized");
                };
                display("info", "status = " + this.status);
            },

            is_end: function() {
                if (this.status == "end") {
                    $(".choice-answer").unbind("click");
                    $(".choice-answer").attr("href", "/users/sign_up");
                    this.write_cookie();
                    return true;
                } else {
                    return false;
                }
            },

            write_cookie: function() {
                console.log(this.result);
                Cookies.set("quiz_result", JSON.stringify(this.result));
                console.log(Cookies.get("quiz_result"));
            },

            display_status: function() {
                display("Status", "Status : " + this.status + " If Yes : " + this.choices[this.status].yes + " If No : " + this.choices[this.status].no);
                display("Status", "text : " + this.choices[this.status].text + " img : " + this.choices[this.status].img);
                var result_string = "";
                for (var i in this.result) {
                    result_string += i + " : " + this.result[i].toString() + " | "
                }
                display("Status", "Result : " + result_string)
            },

            display_html: function() {
                $("#choice-detail").text(this.choices[this.status].text.toString());
                var newSrc = "/assets/quiz/" + this.choices[this.status].img.toString();
                $.ajax({
                    url: newSrc,
                    type: "GET",
                    headers: {
                        "X-TOKEN": 'xxxxx'
                    }
                }).done(function() {
                    $("#choice-img").attr("src", newSrc);
                }).fail(function() {
                    console.log("Ajax failed");
                });

            },

            get_result: function() {
                if (this.is_end()) {
                    return this.result;
                } else {
                    display("error", "Decision Tree not complete");
                }
            }
        };

        var Timer = {
            time: 60,
            status: 0,
            interval: 1,
            color_start: 105,
            color_end: 15,
            progress: function() {
                if (this.status < this.time && this.status >= 0) {
                    this.status += this.interval;
                } else {
                    display("info", "timer end reached");
                    clearInterval(this.loop);
                }
            },
            display_html: function() {
                var w = 100 - (this.status / this.time) * 100;
                var color = this.color_start - (this.color_start - this.color_end) * (this.status / this.time);
                //console.log(color);
                $("#timer-progress").width(w.toString() + "%");
                $("#timer-txt").text(Math.round(this.time - this.status).toString());
                $("#timer-txt").css({ left: 100 - w.toString() + "%" })
                $("#timer-progress").css("background-color", "hsl(" + Math.round(color) + ", 100%, 50%)")
            },
            info: function() {
                display("info", "Status: " + this.status.toString() + " | Time: " + this.time.toString() + " | Interval: " + this.interval.toString() + " | Ratio: " + ((this.status / this.time) * 100).toString() + "%");
            },
            play: function() {
                var timer_used = this;
                var loop = setInterval(function() {
                    timer_used.progress();
                    timer_used.display_html();
                    //timer_used.info();
                    if (timer_used.status >= timer_used.time) {
                        clearInterval(loop);
                        timer_used.info();
                    }
                }, this.interval * 1000);
            }
        };


        ""
        var sport_tree = Object.create(DecisionTree);
        var sport_choices = { "paris": { text: "Habitez-vous Paris ?", img: "paris", yes_id: "woman", no_id: "woman" }, "woman": { text: "Etes-vous......une femme ?", img: "woman", yes_id: "student", no_id: "student" }, "student": { text: "...Etudiant ?", img: "student", yes_id: "agenda", no_id: "freelance" }, "freelance": { text: "...Freelance ?", img: "freelance", yes_id: "agenda", no_id: "startupper" }, "startupper": { text: "...Startupper ?", img: "startupper", yes_id: "agenda", no_id: "salaried" }, "salaried": { text: "...Salarié ?", img: "salaried", yes_id: "agenda", no_id: "agenda" }, "agenda": { text: "Avez-vous planifié certaines des Activités de la semaine qui vient sur une application mobile d'agenda ? (Google Agenda...)", img: "agenda", yes_id: "professional activities", no_id: "3hours" }, "professional activities": { text: "....des activités professionnelles ?", img: "professional activities", yes_id: "personal", no_id: "3hours" }, "personal": { text: "...Des activités personnelles? (ie Médecins, mariages...)", img: "personal_activities", yes_id: "friendly_activities", no_id: "3hours" }, "friendly_activities": { text: "...Activités amicales ou familiales ? (Sport, Cinéma, ...)", img: "friendly_activities", yes_id: "3hours", no_id: "3hours" }, "3hours": { text: "Avez-vous en moyenne des plages horaires non planifiées dans votre calendrier: + de 3h/hour ?", img: "3hours", yes_id: "6hours", no_id: "6hours" }, "6hours": { text: "... : =+ de 6h/hour ?", img: "6hours", yes_id: "google_calendar", no_id: "google_calendar" }, "google_calendar": { text: "L'application que vous utilisez le + est : Google Calendar ?", img: "google_calendar", yes_id: "metro", no_id: "apple_calendar" }, "apple_calendar": { text: "...Le Calendrier Apple ?", img: "apple_calendar", yes_id: "metro", no_id: "metro" }, "metro": { text: "Votre moyen de locomotion le + fréquent est: Le Métro ?", img: "metro", yes_id: "LittleGauge", no_id: "bike" }, "bike": { text: "Moyen de locomotion...le + fréquent : Le vélo ?", img: "bike", yes_id: "LittleGauge", no_id: "car" }, "car": { text: "Moyen de locomotion le + fréquent : La voiture ?", img: "car", yes_id: "LittleGauge", no_id: "LittleGauge" }, "LittleGauge": { text: "Comment évaluez vous votre pratique du sport: plutôt...Faible (à venir medium & high)?", img: "LittleGauge", yes_id: "tennis", no_id: "MediumGauge" }, "MediumGauge": { text: "...Pratique sport : fréquente ?", img: "MediumGauge", yes_id: "tennis", no_id: "HighGauge" }, "HighGauge": { text: "...Pratique sport : haut-niveau ?", img: "HighGauge", yes_id: "tennis", no_id: "tennis" }, "tennis": { text: "Avez-vous pratiqué au cours des 3 derniers mois du : Tennis ?", img: "tennis", yes_id: "squash", no_id: "squash" }, "squash": { text: "Avez-vous pratiqué au cours des 3 derniers mois du : squash ?", img: "squash", yes_id: "musculation", no_id: "musculation" }, "musculation": { text: "Pratiqué...3 derniers mois... Musculation ?", img: "musculation", yes_id: "Travel", no_id: "Travel" }, "Travel": { text: "Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…: par mois ?", img: "travel", yes_id: "travel2", no_id: "travel2" }, "travel2": { text: "Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…: par semaine ?", img: "travel", yes_id: "macOs", no_id: "macOs" }, "macOs": { text: "Système d’exploitation que vous utilisez le + : macOS ?", img: "macOs", yes_id: "iOs", no_id: "windows" }, "windows": { text: "Système d’exploitation que vous utilisez le + : Windows ?", img: "windows", yes_id: "iOs", no_id: "iOs" }, "iOs": { text: "OS Mobile que vous utilisez le + : iOS?", img: "ios", yes_id: "Free", no_id: "Free" }, "Free": { text: "Opérateur mobile : Free ?", img: "free", yes_id: "Coding", no_id: "Coding" }, "Coding": { text: "Avez-vous déjà... codé ?", img: "Coding", yes_id: "Back-end", no_id: "improvisation" }, "Front-end": { text: "plutôt front-end... ?", img: "Back-end", yes_id: "Back-end", no_id: "RubyOnRails" }, "Back-end": { text: "Full-Stack ?", img: "Front-end", yes_id: "RubyOnRails", no_id: "improvisation" }, "RubyOnRails": { text: "RubyOnRails ?", img: "RubyOnRails", yes_id: "sql", no_id: "sql" }, "sql": { text: "SQL ?", img: "sql", yes_id: "MachineLearning", no_id: "MachineLearning" }, "MachineLearning": { text: "MachineLearning ?", img: "MachineLearning", yes_id: "improvisation", no_id: "improvisation" }, "improvisation": { text: "Avez-vous déjà fait...: du théâtre d'improvisation ?", img: "improvisation", yes_id: "crowdfunding", no_id: "crowdfunding" }, "crowdfunding": { text: "Avez-vous déjà fait...: une campagne de Crowdfunding?", img: "crowdfunding", yes_id: "Mobile App Development", no_id: "Mobile App Development" }, "Mobile App Development": { text: "Avez-vous déjà fait...: une Application mobile ?", img: "Mobile_App_Development", yes_id: "cinema", no_id: "cinema" }, "cinema": { text: "Avez-vous déjà fait... au cours des 6 derniers mois...une sortie cinéma ?", img: "cinema", yes_id: "Trust2", no_id: "Trust2" }, "Trust2": { text: "Etes-vous d'accord pour partager informations précédentes avec la communauté NowFellows et être éventuellement mis en relation avec des personnes qui vous correspondent ?", img: "Trust2", yes_id: "Trust", no_id: "Trust" }, "Trust": { text: "Etes-vous intéressé pour partager/enseigner une compétence avec d'autres early-adopters de NowFellows lors d'un évènement à Paris Bastille ⛵ ?", img: "Trust", yes_id: "couple", no_id: "couple" }, "couple": { text: "Etes-vous en couple ? Pour vous partager des idées de sorties, activités, cadeaux...ou des propositions d'éventuelles opportunités de rencontre si ce n'est pas le cas. Seul le coach de NowFellows a accès à cette information", img: "couple", yes_id: "LearnToLearn", no_id: "LearnToLearn" }, "LearnToLearn": { text: "Intéressé par apprendre de nouvelles compétences ?", img: "LearnToLearn", yes_id: "end", no_id: "end" }, "end": { text: "Merci d'avoir répondu à ce questionnaire, cliquez sur une des 2 flèches pour découvrir la suite ;)", img: "end", yes_id: "end", no_id: "end" } };

        /*{
            "sport": {
                text: "Faites-vous régulièrement du sport?",
                img: "sport.png",
                yes: "sport-co",
                no: "sport-proposition"
            },
            "sport-co": {
                text: "Faites-vous plutôt du sport en équipe?",
                img: "sport-co.png",
                yes: "football",
                no: "squash"
            },
            "sport-proposition": {
                text: "Souhaitez-vous trouver qqun pour vous motiver à faire du sport ?",
                img: "sport-ami.png",
                yes: "sport-co",
                no: "end"
            },
            "football": {
                text: "Pratiquez-vous le Football ?",
                img: "foot.png",
                yes: "end",
                no: "end"
            },
            "squash": {
                text: "Pratiquez-vous le Squash ?",
                img: "squash.png",
                yes: "end",
                no: "end"
            },
            "end": {
                text: "Merci d'avoir participé au quiz, cliquez sur l'une des flèches pour vous inscrire et découvrir ce que nous vous réservons",
                img: "end.png",
                yes: "end",
                no: "end"
            }
        };*/
        var sport_initial = "paris";
        var sport_result = {};
        var sport_status = "paris";

        var timer_test = Object.create(Timer);
        timer_test.time = 180;
        timer_test.interval = 0.01;
        timer_test.status = 0;

        $('.choice-answer').click(function(e) {
            e.preventDefault();
            $('.choice-answer').unbind("click");
            sport_tree.init(sport_initial, sport_choices, sport_status, sport_result);
            sport_tree.display_html();
            timer_test.play();
        });
        console.log("test.js loaded");
    });
>>>>>>> f13b3e67d9c19a397d892a6fe0e273b65f5a43a7
};

$(document).ready(ready);
$(document).on('page:load', ready);
<<<<<<< HEAD
$(document).on('page:change', ready);
=======
$(document).on('page:change', ready);
>>>>>>> f13b3e67d9c19a397d892a6fe0e273b65f5a43a7
