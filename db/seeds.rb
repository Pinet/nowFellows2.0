# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

20.times { |n|

nom = Faker::Name.last_name
prenom = Faker::Name.first_name
email = prenom + "." + nom + "@test.com"

user = User.new({nom: nom, prenom: prenom, email: email, password: 'password', password_confirmation: 'password'})
user.skip_confirmation!
user.save

}


arbre = [{id: "paris",text: "Habitez-vous Paris ?",img: "paris",yes_id: "woman",no_id: "woman"},
{id:"woman",text:"Etes-vous......une femme ?",img:"woman",yes_id:"student",no_id:"student"},
{id:"student",text:"...Etudiant ?",img:"student",yes_id:"agenda",no_id:"freelance"},
{id:"freelance",text:"...Freelance ?",img:"freelance",yes_id:"agenda",no_id:"startupper"},
{id:"startupper",text:"...Startupper ?",img:"startupper",yes_id:"agenda",no_id:"salaried"},
{id:"salaried",text:"...Salarié ?",img:"salaried",yes_id:"agenda",no_id:"agenda"},
{id:"agenda",text:"Avez-vous planifiez certaines des Activités de la semaine qui vient sur une application mobile d'agenda ? (Google Agenda...)",img:"agenda",yes_id:"professional activities",no_id:"3hours"},
{id:"professional activities",text:"....des activités professionnelles ?",img:"professional activities",yes_id:"personal",no_id:"3hours"},
{id:"personal",text:"...Des activités personnelles ie Médecins, mariages...?",img:"personal",yes_id:"friendly activities",no_id:"3hours"},
{id:"friendly activities",text:"...Activités amicales ou familiales ? (Sport, Cinéma, ...)",img:"friendly activities",yes_id:"3hours",no_id:"3hours"},
{id:"3hours",text:"Avez-vous en moyenne des plages horaires non planifiées dans votre calendrier: + de 3h/hour ?",img:"3hours",yes_id:"6hours",no_id:"6hours"},
{id:"6hours",text:"... : =+ de 6h/hour ?",img:"6hours",yes_id:"google calendar",no_id:"google calendar"},
{id:"google calendar",text:"L'application que vous utilisez le + est :\nGoogle Calendar ?",img:"google calendar",yes_id:"metro",no_id:"apple calendar"},
{id:"apple calendar",text:"...Le Calendrier Apple ?",img:"apple calendar",yes_id:"metro",no_id:"metro"},
{id:"metro",text:"Votre moyen de locomotion le + fréquent est: Le Métro ?",img:"metro",yes_id:"LittleGauge",no_id:"bike"},
{id:"bike",text:"Moyen...locomotion...le + fréquent : vélo ?",img:"bike",yes_id:"LittleGauge",no_id:"car"},
{id:"car",text:"Moyen...locomotion le + fréquent :  voiture ?",img:"car",yes_id:"LittleGauge",no_id:"LittleGauge"},
{id:"LittleGauge",text:"Comment évaluez vous votre pratique du sport: plutôt...Faible (à venir medium & high)?",img:"LittleGauge",yes_id:"tennis",no_id:"MediumGauge"},
{id:"MediumGauge",text:"...Pratique sport:\n
Fréquente ?",img:"MediumGauge",yes_id:"tennis",no_id:"HighGauge"},
{id:"HighGauge",text:"...Pratique sport:\n
Haut-niveau ?",img:"HighGauge",yes_id:"tennis",no_id:"tennis"},
{id:"tennis",text:"Avez-vous pratiqué au cours des 3 derniers mois du :\n
Tennis ?",img:"tennis",yes_id:"squash",no_id:"squash"},
{id:"squash",text:"Avez-vous pratiqué au cours des 3 derniers mois du :\n
Squash ?",img:"squash",yes_id:"musculation",no_id:"musculation"},
{id:"musculation",text:"Pratiqué...3 derniers mois...  :\n
Musculation ?",img:"musculation",yes_id:"Travel ",no_id:"Travel "},
{id:"Travel ",text:"Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…:\nPar mois ?",img:"Travel ",yes_id:"travel2",no_id:"travel2"},
{id:"travel2",text:"Au cours des 3 derniers mois, avez-vous voyagé à l’étranger une ou plusieurs fois…:\nPar semaine ?",img:"travel ",yes_id:"macOs",no_id:"macOs"},
{id:"macOs",text:"Système d’exploitation que vous utilisez le + :\nMacOS ?",img:"macOs",yes_id:"iOs",no_id:"windows"},
{id:"windows",text:"Système d’exploitation que vous utilisez le + :\n
Windows ?",img:"windows",yes_id:"iOs",no_id:"iOs"},
{id:"iOs",text:"OS Mobile que vous utilisez le + :\n
iOS?",img:"iOs",yes_id:"Free ",no_id:"Free "},
{id:"Free ",text:"Opérateur mobile :\n
Free ?",img:"Free ",yes_id:"Coding",no_id:"Coding"},
{id:"Coding",text:"Avez-vous déjà... Coder ?",img:"Coding",yes_id:"Back-end",no_id:"improvisation "},
{id:"Front-end",text:"plutôt front-end... ?",img:"Back-end",yes_id:"Back-end",no_id:"RubyOnRails"},
{id:"Back-end",text:"Full-Stack ?",img:"Front-end",yes_id:"RubyOnRails",no_id:"improvisation "},
{id:"RubyOnRails",text:"RubyOnRails ?",img:"RubyOnRails",yes_id:"sql",no_id:"sql"},
{id:"sql",text:"SQL ?",img:"sql",yes_id:"MachineLearning",no_id:"MachineLearning"},
{id:"MachineLearning",text:"MachineLearning ?",img:"MachineLearning",yes_id:"improvisation ",no_id:"improvisation "},
{id:"improvisation ",text:"Avez-vous déjà fait...:\n
Du théâtre d'improvisation ?",img:"improvisation ",yes_id:"crowdfunding",no_id:"crowdfunding"},
{id:"crowdfunding",text:"Avez-vous déjà fait...:\n
Campagne de Crowdfunding?",img:"crowdfunding",yes_id:"Mobile App Development",no_id:"Mobile App Development"},
{id:"Mobile App Development",text:"Avez-vous déjà fait...:\n
une Application mobile ?",img:"Mobile App Development",yes_id:"cinema",no_id:"cinema"},
{id:"cinema",text:"Avez-vous déjà fait... au cours des 6 derniers mois...:\n
Une sortie cinéma ?",img:"cinema",yes_id:"Trust2",no_id:"Trust2"},
{id:"Trust2",text:"Etes-vous d'accord pour partager informations précédentes avec la communauté NowFellows et être éventuellement mis en relation avec des personnes qui vous correspondent ?",img:"Trust2",yes_id:"Trust",no_id:"Trust"},
{id:"Trust",text:"Etes-vous intéressé pour partager/enseigner une compétence avec d'autres early-adopters de NowFellows lors d'un évènement à Paris Bastille ?",img:"Trust",yes_id:"couple",no_id:"couple"},
{id:"couple",text:"Etes-vous en couple ?  (Pour vous partager des idées de sorties, activités,  cadeaux...ou des propositions d'éventuelles opportunités de rencontre si ce n'est pas le cas) Seul le coach de NowFellows a accès à cette information)",img:"couple",yes_id:"LearnToLearn",no_id:"LearnToLearn"},
{id:"LearnToLearn",text:"Intéressé par apprendre de nouvelles compétences ?",img:"LearnToLearn",yes_id:"end",no_id:"end"},
{id:"end",text:"Merci d'avoir répondu à ce questionnaire, cliquez sur une des 2 flèches pour découvrir la suite ;)",img:"end",yes_id:"end",no_id:"end"}]
 

arbre.each { |a|
	Keyword.create(id: a[:id].to_s, text: a[:text].to_s, img: a[:img].to_s, yes_id: a[:yes_id].to_s, no_id: a[:no_id].to_s);
}
