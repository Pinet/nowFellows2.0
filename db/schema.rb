# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

<<<<<<< HEAD
ActiveRecord::Schema.define(version: 20170919151744) do

  create_table "conversations", force: :cascade do |t|
    t.integer "recipient_id"
    t.integer "sender_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_id", "sender_id"], name: "index_conversations_on_recipient_id_and_sender_id", unique: true
    t.index ["recipient_id"], name: "index_conversations_on_recipient_id"
    t.index ["sender_id"], name: "index_conversations_on_sender_id"
  end
=======
ActiveRecord::Schema.define(version: 20170914125705) do
>>>>>>> f13b3e67d9c19a397d892a6fe0e273b65f5a43a7

  create_table "keywords", id: false, force: :cascade do |t|
    t.string "id", null: false
    t.string "text"
    t.string "img"
    t.integer "skill"
    t.integer "interest"
    t.string "yes_id"
    t.string "no_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_keywords_on_id", unique: true
    t.index ["no_id"], name: "index_keywords_on_no_id"
    t.index ["yes_id"], name: "index_keywords_on_yes_id"
  end

<<<<<<< HEAD
  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.integer "user_id"
    t.integer "conversation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

=======
>>>>>>> f13b3e67d9c19a397d892a6fe0e273b65f5a43a7
  create_table "users", force: :cascade do |t|
    t.string "nom", default: "", null: false
    t.string "prenom", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
<<<<<<< HEAD
=======
    t.string "keywords"
>>>>>>> f13b3e67d9c19a397d892a6fe0e273b65f5a43a7
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "linkedin_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["nom"], name: "index_users_on_nom"
    t.index ["prenom"], name: "index_users_on_prenom"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
